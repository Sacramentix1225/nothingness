package customShader;

class ScreenEffect extends hxsl.Shader {

	static var SRC = {

		@global var time : Float;
		@param var intensity : Float;
		@param var absorptionR : Float;
		@param var absorptionG : Float;
        @param var absorptionB : Float;
        @param var speed : Float;

		var calculatedUV : Vec2;
        var pixelColor : Vec4;

		function fragment() {
			var i = vec3(intensity, intensity, intensity);
            var a = vec3(absorptionR, absorptionG, absorptionB);
            var pLeft = vec3(0.0, sin(time/1.53*speed)*0.5 + 0.5, -0.1*(sin(time/1.14*speed)*0.5+0.5));
            var pRight = vec3(1.0, sin(time/2.785*speed)*0.5 + 0.5, -0.1*(sin(time/1.871*speed)*0.5+0.5));
            var pL = mix(pLeft, pRight, calculatedUV.x);
            var pS = vec3(calculatedUV, 0.0);
            var col = i * exp(-a * 4.0 * (distance(pLeft, pL) + distance(pL, pS)));
            
            pixelColor = vec4(col,1.0);
		}

	};

	public function new() {
		super();
		this.intensity = 0.1;
		this.absorptionR = 0.17;
        this.absorptionG = 0.9;
        this.absorptionB = 0.02;
        this.speed = 1.0;
	}

}
