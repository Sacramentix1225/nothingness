package gameObject;

import sound.SoundManager;
import hxd.Res;
import h2d.SpriteBatch;
import scene.Playground;
import gameObject.Hero;
import h2d.col.Point;

class Collision {

    public static function collect(hero:Hero, object:Collectable) {
        if (hero.body.getBounds().intersects(object.getBounds())) {
            object.onCollect(hero);
            Game.currentScene.removeChild(object);
            Playground.collectables.remove(object);
            object = null;
        }
    }

    public static function bulletsHit(hero:Hero, ennemies:SpriteBatch) {
        for ( ennemy in ennemies.getElements()) {
            for ( bullet in hero.bullets.getElements()) {
                try {
                    if ( cast(ennemy, Ennemy).getHitbox().contains(new Point(bullet.x, bullet.y))  ) {
                        //bullet.remove();
                        //bullet = null;
                        cast(ennemy, Ennemy).hp -= hero.gunDamage;

                        if (cast(ennemy, Ennemy).hp <= 0) {
                            switch(Std.random(3)) {
                                case 0:
                                    SoundManager.getInstance().trigger(Res.sound.kill0);
                                case 1:
                                    SoundManager.getInstance().trigger(Res.sound.kill1);
                                case 2:
                                    SoundManager.getInstance().trigger(Res.sound.kill2);
                            }
                            
                            ennemy.remove();
                            ennemy = null;
                        } else {
                            SoundManager.getInstance().trigger(Res.sound.hit);
                        }

                    }
                } catch (e :Dynamic) {

                }
            }
        }
    }

    public static function hitHero(ennemies:SpriteBatch, hero:Hero) {
        for ( ennemy in ennemies.getElements()) {
            try {
                if ( cast(ennemy, Ennemy).canAttack && cast(ennemy, Ennemy).getHitbox().intersects(hero.body.getBounds()) ) {
                    hero.hp -= cast(ennemy, Ennemy).damage;
                    cast(ennemy, Ennemy).onAttack();
                    if ( hero.hp <= 0) {
                        //hero.death();
                        SoundManager.getInstance().trigger(Res.sound.death);
                    } else {
                        switch(Std.random(3)) {
                                case 0:
                                    SoundManager.getInstance().trigger(Res.sound.hurt0);
                                case 1:
                                    SoundManager.getInstance().trigger(Res.sound.hurt1);
                                case 2:
                                    SoundManager.getInstance().trigger(Res.sound.hurt2);
                            }
                    }
                }
            } catch (e :Dynamic) {

            }   
        }
    }

}