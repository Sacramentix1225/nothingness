package gameObject;


import motion.Actuate;
import h2d.Tile;
import h2d.col.Bounds;
import hxd.Window;
import hxd.Math;
import gameObject.Hero;
import h2d.SpriteBatch.BatchElement;

class Ennemy extends BatchElement {

    public var speed:Float = 25;
    public var hero:Hero;
    public var hp:Int = 150;
    public var bounds:Bounds;
    public var damage:Int = 25;
    public var canAttack:Bool = true;
    public var attackSpeed:Float = 1;

    public function new(t:Tile) {
        super(t);
        bounds = new Bounds();
        bounds.xMin = x -(Window.getInstance().height*0.035*0.5);
        bounds.xMax = x +(Window.getInstance().height*0.035*0.5);
        bounds.yMin = y -(Window.getInstance().height*0.035*0.5);
        bounds.yMax = y +(Window.getInstance().height*0.035*0.5);
    }

	override function update(dt:Float) {
        var dx = hero.localToGlobal().x-x;
        var dy = hero.localToGlobal().y-y;
        var angle = Math.atan2(dy, dx);
		x += speed*dt*Math.cos(angle);
		y += speed*dt*Math.sin(angle);
        rotation = angle;
        bounds.xMin = x -(Window.getInstance().height*0.035*0.5);
        bounds.xMax = x +(Window.getInstance().height*0.035*0.5);
        bounds.yMin = y -(Window.getInstance().height*0.035*0.5);
        bounds.yMax = y +(Window.getInstance().height*0.035*0.5);
		return true;
	}

    public function getHitbox():Bounds {
        return bounds;
        
    }

    public function onAttack() {
        canAttack = false;
        Actuate.timer(attackSpeed).onComplete(function () {
            canAttack = true;
        });
    }

}