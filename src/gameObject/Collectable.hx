package gameObject;

import h2d.Tile;
import h2d.Bitmap;

class Collectable extends Bitmap {
    


    public function new(onCollect:Hero->Void , width:Float , height:Float, ?tile:Tile) {
        super();
        if ( tile == null ) {
            this.tile = Tile.fromColor(0x654321);
        } else {
            this.tile = tile;
        }
        this.tile.scaleToSize(width, height);
        this.tile.setCenterRatio();
        this.onCollect = onCollect;
    }

    public dynamic function onCollect(hero:Hero) {
    }

}