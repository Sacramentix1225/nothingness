package gameObject;

import hxd.Res;
import sound.SoundManager;
import motion.Actuate;
import h2d.col.Point;
import h2d.Object;
import h2d.Bitmap;
import h2d.Tile;
import hxd.Window;
import hxd.Math;
import hxd.Key as K;
import tools.KeyConfig;
import story.Chapter;
import h2d.SpriteBatch;
import addons.Upgrade;
import pattern.Observer.NotifPack;
using pattern.Observer.Observables;



class Hero extends Object {
    
    public var moveKeyID : { left : Int, right : Int, up : Int, down : Int};
    public var moveKey : Array<Int>;

    public var gunKeyID : { fire : Int, rotation : Int, antiRotation : Int};
    public var gunKey : Array<Int>;

    public var body:Bitmap;

    public var gun:Bitmap;

    public var gunFireRate:Float = 0.05;

    public var canFire:Bool = true;

    public var gunDamage:Int = 10;

    public var bullets:SpriteBatch;

    public var maxhp:Int = 500;
    public var hp (default, set) :Int = 500;

    public function set_hp(d:Int) {
        var r = if (d<0) 0  else d;
	    this.notify(new NotifPack(r, 0));
	    return hp = r;
        
    }

    public var speed: Int = 250;

    public function new(width, height) {
        super();
        this.body = new Bitmap(Tile.fromColor(0x282b2b, width, height), this); //383b3e
        this.body.tile.setCenterRatio();
        
        this.x = Window.getInstance().width*0.5;
        this.y = Window.getInstance().height*0.5;

        bullets = new SpriteBatch(Tile.fromColor(0x282b2b));
        bullets.hasUpdate = true;
        Game.currentScene.addChild(bullets);

        moveKey = new Array<Int>();
        var shuffleKey = new Array<Int>();
        shuffleKey = [for (i in 0...4) i];
        Math.shuffle(shuffleKey);
        moveKeyID = {
			left : shuffleKey[0] ,
			right : shuffleKey[1] ,
			up : shuffleKey[2] ,
			down : shuffleKey[3] ,
			
		};

        gunKey = new Array<Int>();
        shuffleKey = new Array<Int>();
        shuffleKey = [for (i in 0...3) i];
        Math.shuffle(shuffleKey);
        gunKeyID = {
			fire : shuffleKey[0] ,
			rotation : shuffleKey[1] ,
			antiRotation : shuffleKey[2] ,
			
		};

    }
    public function update(dt:Float) {
        try {
            if (moveUp()) {
                y -= dt*speed;
            }
            if (moveDown()) {
                y += dt*speed;
            }
            if (moveLeft()) {
                x -= dt*speed;
            }
            if (moveRight()) {
                x += dt*speed;
            }
            if ( Upgrade.getInstance().realControl ) {
                var dx = Window.getInstance().mouseX-localToGlobal().x;
                var dy = Window.getInstance().mouseY-localToGlobal().y;
                var angle = Math.atan2(dy, dx);
                gun.rotation = angle-Math.degToRad(90);
            } else {
                if (antiRotate()) {
                    gun.rotate(-dt*5);
                }
                if (normalRotate()) {
                    gun.rotate(dt*5);
                }
            }
            if ( fire() && canFire ) {
                var bullet = new BasicElement(Tile.fromColor(0xff0000, Std.int(Window.getInstance().height*0.005), Std.int(Window.getInstance().height*0.005)));
                bullet.x = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).x;
                bullet.y = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).y;
                bullet.vx = -550* Math.sin(gun.rotation);
                bullet.vy = 550 * Math.cos(gun.rotation);
                bullet.t.setCenterRatio();
                bullets.add(bullet);
                if ( Upgrade.getInstance().gunLevel >= 1) {
                    var bullet1 = new BasicElement(Tile.fromColor(0xff0000, Std.int(Window.getInstance().height*0.005), Std.int(Window.getInstance().height*0.005)));
                    var bullet2 = new BasicElement(Tile.fromColor(0xff0000, Std.int(Window.getInstance().height*0.005), Std.int(Window.getInstance().height*0.005)));
                    bullet1.x = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).x;
                    bullet1.y = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).y;
                    bullet2.x = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).x;
                    bullet2.y = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).y;
                    bullet1.t.setCenterRatio();
                    bullet2.t.setCenterRatio();
                    bullet1.vx = -550* Math.sin(gun.rotation-0.26);
                    bullet1.vy = 550 * Math.cos(gun.rotation-0.26);
                    bullet2.vx = -550* Math.sin(gun.rotation+0.26);
                    bullet2.vy = 550 * Math.cos(gun.rotation+0.26);
                    bullets.add(bullet1);
                    bullets.add(bullet2);
                    Actuate.timer(15).onComplete( function () {
                        bullet1.remove();
                        bullet1 = null;
                        bullet2.remove();
                        bullet2 = null;
                    });

                }
                if ( Upgrade.getInstance().gunLevel >= 2) {
                    var bullet1 = new BasicElement(Tile.fromColor(0xff0000, Std.int(Window.getInstance().height*0.005), Std.int(Window.getInstance().height*0.005)));
                    var bullet2 = new BasicElement(Tile.fromColor(0xff0000, Std.int(Window.getInstance().height*0.005), Std.int(Window.getInstance().height*0.005)));
                    bullet1.x = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).x;
                    bullet1.y = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).y;
                    bullet2.x = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).x;
                    bullet2.y = gun.localToGlobal(new Point(0, Std.int(Window.getInstance().height*0.035)*1.5)).y;
                    bullet1.t.setCenterRatio();
                    bullet2.t.setCenterRatio();
                    bullet1.vx = -550* Math.sin(gun.rotation-0.15);
                    bullet1.vy = 550 * Math.cos(gun.rotation-0.15);
                    bullet1.friction = 0.985;
                    bullet2.vx = -550* Math.sin(gun.rotation+0.15);
                    bullet2.vy = 550 * Math.cos(gun.rotation+0.15);
                    bullet2.friction = 0.985;
                    bullets.add(bullet1);
                    bullets.add(bullet2);
                    Actuate.timer(15).onComplete( function () {
                        bullet1.remove();
                        bullet1 = null;
                        bullet2.remove();
                        bullet2 = null;
                    });

                }
                if ( Upgrade.getInstance().soundEngine) {
                    SoundManager.getInstance().trigger(Res.sound.shot);
                }

                Actuate.timer(15).onComplete( function () {
                    bullet.remove();
                    bullet = null;
                });
                canFire = false;
                Actuate.timer(gunFireRate).onComplete( function () {
                    canFire = true;
                });
            }
        } catch (e:Dynamic) {

        }
    }

    public function moveUp():Bool {
        if ( Upgrade.getInstance().realControl ) return K.isDown(K.Z) || K.isDown(K.W) || K.isDown(K.UP);
        return K.isDown(moveKey[moveKeyID.up]);
    }

    public function moveDown():Bool {
        if ( Upgrade.getInstance().realControl ) return K.isDown(K.S) || K.isDown(K.DOWN);
        return K.isDown(moveKey[moveKeyID.down]);
    }
    
    public function moveLeft():Bool {
        if ( Upgrade.getInstance().realControl ) return K.isDown(K.Q) || K.isDown(K.A) || K.isDown(K.LEFT);
        return K.isDown(moveKey[moveKeyID.left]);
    }

    public function moveRight():Bool {
        if ( Upgrade.getInstance().realControl ) return K.isDown(K.D) || K.isDown(K.RIGHT);
        return K.isDown(moveKey[moveKeyID.right]);
    }
    
    public function fire():Bool {
        if ( Upgrade.getInstance().realControl ) return K.isDown(K.MOUSE_LEFT);
        return K.isDown(gunKey[gunKeyID.fire]);
    }

    public function antiRotate():Bool {
        return K.isDown(gunKey[gunKeyID.antiRotation]);
    }
    
    public function normalRotate():Bool {
        return K.isDown(gunKey[gunKeyID.rotation]);
    }

    public function setMoveKey(key:Int) {
        if ( moveKey.length !=4 ) {
                for (i in 0...4 ) {
                    if (moveKey.length == i) {
                        if (!KeyConfig.getInstance().keyIsBind(key)) {
                            moveKey.push(key);
                            KeyConfig.getInstance().add(key);
                        }
                        break;
                    }
                }
                if ( moveKey.length == 4 ) Chapter.getInstance().moveKeySet = true;
            }
    }

    public function receiveGun() {
        gun = new Bitmap(Tile.fromColor(0xac3235, Std.int(Window.getInstance().height*0.012), Std.int(Window.getInstance().height*0.035)), this);
        gun.tile.setCenterRatio(0.5, -0.5);
        Chapter.getInstance().crateRecover = true;
    }

    public function setGunKey(key:Int) {
        if ( gunKey.length !=3 ) {
            for (i in 0...3 ) {
                if (gunKey.length == i) {
                    if (!KeyConfig.getInstance().keyIsBind(key)) {
                        gunKey.push(key);
                        KeyConfig.getInstance().add(key);
                    }
                    break;
                }
            }
                if ( gunKey.length == 3 ) Chapter.getInstance().gunKeySet = true;    
               
        }
    }

}