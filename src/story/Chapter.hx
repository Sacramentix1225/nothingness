package story;


class Chapter {

    private static var instance:Chapter;

    public static function getInstance():Chapter {
        if(instance == null) {
            instance = new Chapter();
        }
        return instance;
    }

    public var inUpgradeShop:Bool = false;

    public var heroSpawn:Bool = false;
    public var moveKeySet:Bool = false;
    public var crateSpawn:Bool = false;
    public var crateRecover:Bool = false;
    public var gunKeySet:Bool = false;
    public var round1:Bool = false;
    public var round1Spawned = false;
    public var round1Complete = false;
    public var round = 2;
    public var actualRound = 0;
    public var roundSpawned:Bool;

    private function new() {
        
    }

}