package tools;



class KeyStorage {
    
    private var storage:Array<Int>;
    public var maxLength:Int;

    public function new(maxLength) {
        this.maxLength = maxLength;
        storage = new Array<Int>();
        storage = [for (i in 0...maxLength) -1];

    }

    public function add(key:Int) {
        storage.pop();
        storage.unshift(key);
    }

}