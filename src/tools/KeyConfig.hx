package tools;

import hxd.Key as K;

class KeyConfig {

    private static var instance:KeyConfig;

    public static function getInstance():KeyConfig {
        if(instance == null) {
            instance = new KeyConfig();
        }
        return instance;
    }

    private var keybindList:Array<Int>;

    private function new() {
        keybindList = new Array<Int>();
        keybindList.push(K.ESCAPE);
    }

    public function add(key:Int) {
        keybindList.push(key);
    }

    public function keyIsBind(key:Int):Bool {
        if (keybindList.indexOf(key) != -1) return true;
        return false;
    }

}