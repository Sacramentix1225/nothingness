package scene;

import haxe.MainLoop;
import h2d.col.Point;
import h2d.filter.Shader;
import sound.SoundManager;
import hxd.res.DefaultFont;
import h2d.Text;
import motion.Actuate;
import h2d.SpriteBatch;
import hxd.Math;
import gameObject.Collectable;
import hxd.Event;
import story.Chapter;
import h2d.Bitmap;
import h2d.Scene;
import hxd.Window;
import h2d.Tile;
import hxd.Key as K;
import gameObject.Hero;
import tools.KeyStorage;
import tools.KeyConfig;
import gameObject.Ennemy;
import addons.AddonsSelector;
import addons.Upgrade;
import customShader.ScreenEffect;

using gameObject.Collision;

    /**
    * Class to create a screen display of the main menu ( displayed when you start the game ).
    * You can insert all UI element that you want to display. 
    * You can find preBuild UI element in scene.gameUI
    */

class Playground extends DynamicScene  {
    private var scene:Scene;
    private var background:h2d.Bitmap;
    public var lastTimeStamp:Float;
    public static var hero:Hero;
    public var keyBuffer:KeyStorage;
    
    public static var collectables:Array<Collectable>;
    public static var ennemies:SpriteBatch;

    public var canSpawnT1:Bool = true;

    public var updateEvent:MainEvent;

    /**
     * Create a new scene of the options menu
     * if you want to display it don't forget to change the current scene of the game with Game.instance.setScene( insert the new scene )
     */

    public function new() {
        super();
        
        Chapter.getInstance();
        KeyConfig.getInstance();
        Upgrade.getInstance();
        SoundManager.init();
        
        Upgrade.getInstance().realControl = true;
        Upgrade.getInstance().soundEngine = true;
        Upgrade.getInstance().gunLevel = 2;
        Chapter.getInstance().round = 100;

        //Playground.hero.filter = new h2d.filter.Glow(0x20b2aa, 0.5, 20, 5, 1, true);
        //Playground.hero.bullets.filter = new h2d.filter.Glow(0x20b2aa, 0.5, 20, 5, 1, true);
        

        this.width = Window.getInstance().width;
        this.height = Window.getInstance().height;

        var bg = Tile.fromColor(0x0e0f0f);//f2eecb
        bg.scaleToSize(width, height);
        background = new Bitmap(bg, this);
        //var shader = new ScreenEffect();
        //background.addShader(shader);
        keyBuffer = new KeyStorage(10);

        function onEvent(event : Event) {
            switch(event.kind) {
                case EKeyUp: keyRelease(event);
                default:
            }
        }
        Window.getInstance().addEventTarget(onEvent);
        

        collectables = new Array<Collectable>();
        ennemies = new SpriteBatch(Tile.fromColor(0xac3235));
        ennemies.hasUpdate = true;
        addChild(ennemies);
        Playground.ennemies.filter = new h2d.filter.Glow(0xbc3235, 0.5, 500, 4, 1, true);
        //lastTimeStamp = haxe.Timer.stamp();
        //updateEvent = MainLoop.add(updatePerTick);

        

    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */


    override public function onResize() {

        width = Window.getInstance().width;
        height = Window.getInstance().height;

        background.tile.scaleToSize(width, height);

        
        
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    override public function update(dt:Float) {
        //var timeStamp = haxe.Timer.stamp();
        //var dt = timeStamp-lastTimeStamp;
        if (Chapter.getInstance().inUpgradeShop) {
            AddonsSelector.getInstance().update(dt);
            return;
        }

        for (object in collectables) {
            hero.collect(object);
        }

        hero.bulletsHit(ennemies);
        ennemies.hitHero(hero);

        if (Chapter.getInstance().heroSpawn) {
            hero.update(dt);
        }
        if ( Chapter.getInstance().moveKeySet && !Chapter.getInstance().crateSpawn ) {
            Chapter.getInstance().crateSpawn = true;
            var onCollect = function (hero:Hero) {
                hero.receiveGun();
            };

            var crate = new Collectable(onCollect, height*0.035, height*0.035);
            crate.x = Math.random()*width;
            crate.y = Math.random()*height;
            addChild(crate);
            collectables.push(crate);
            
        }
        if ( Chapter.getInstance().gunKeySet && !Chapter.getInstance().round1 ) {
            Chapter.getInstance().round1 = true;
            Actuate.timer(2).repeat(10).onRepeat(
                function () {
                    var ennemy = new Ennemy(Tile.fromColor(0xff0000, Std.int(height*0.035), Std.int(height*0.035)));

                    var p = randomSpawnPoint();
                    ennemy.x = p.x;
                    ennemy.y = p.y;
                    ennemy.hero = hero;
                    ennemy.t.setCenterRatio();
                    ennemies.add(ennemy);
                }
            ).onComplete( function () {
                Chapter.getInstance().round1Spawned = true;
            });
            
        }

        if (Chapter.getInstance().round1Spawned && ! Chapter.getInstance().round1Complete) {
            if ( ennemies.isEmpty()) {
                hero.hp = hero.maxhp;
                Chapter.getInstance().round1Complete = true;
                var congratText = new Text(DefaultFont.get(),this);
                congratText.text = " Round 1 complete";
                congratText.textAlign = Center;
                congratText.textColor = 0x383b3e;
                congratText.x = 0.5*width;
                congratText.y = 0.5*height;
                congratText.scale(10);
                Actuate.tween(congratText, 3, {alpha : 0}).onComplete( function () {
                    congratText.remove();
                    congratText = null;
                });
                addChild(AddonsSelector.getInstance());
                Chapter.getInstance().inUpgradeShop = true;
                return;
            }
        }
        if (Chapter.getInstance().round1Complete && ( Chapter.getInstance().actualRound != Chapter.getInstance().round )) {
            Chapter.getInstance().actualRound = Chapter.getInstance().round;
            var cd = 1.5/Math.pow(1.05, Chapter.getInstance().actualRound)+0.0001;
            var spawn = Std.int(10 +Chapter.getInstance().actualRound*Chapter.getInstance().actualRound*0.2);
            Actuate.timer(cd).repeat(spawn).onRepeat(
                function () {
                    var ennemy = new Ennemy(Tile.fromColor(0xff0000, Std.int(height*0.035), Std.int(height*0.035)));
                    var p = randomSpawnPoint();
                    ennemy.x = p.x;
                    ennemy.y = p.y;
                    ennemy.hero = hero;
                    ennemy.t.setCenterRatio();
                    var hp = Std.int(150 + 10*Chapter.getInstance().actualRound);
                    if (hp > 300) hp = 300;
                    ennemy.hp = hp;
                    var speed = Std.int(25 + 10*Chapter.getInstance().actualRound);
                    if (speed > 200) speed = 200;
                    ennemy.speed = speed;
                    ennemies.add(ennemy);
                }
            ).onComplete( function () {
                Chapter.getInstance().roundSpawned = true;
            });

        }

        if (Chapter.getInstance().round1Complete && Chapter.getInstance().roundSpawned) {
            if ( ennemies.isEmpty()) {
                hero.hp = hero.maxhp;
                Chapter.getInstance().roundSpawned = false;
                var congratText = new Text(DefaultFont.get(),this);
                congratText.text = " Round " + Chapter.getInstance().actualRound + "complete";
                congratText.textAlign = Center;
                congratText.textColor = 0x383b3e;
                congratText.x = 0.5*width;
                congratText.y = 0.5*height;
                congratText.scale(10);
                Actuate.tween(congratText, 3, {alpha : 0}).onComplete( function () {
                    congratText.remove();
                    congratText = null;
                });
                addChild(AddonsSelector.getInstance());
                Chapter.getInstance().inUpgradeShop = true;
                Chapter.getInstance().round++;
                return;
            }
        }

    }

    public function keyRelease(e:Event) {
        
        var key = e.keyCode;

        if (key == K.ESCAPE && hero == null) {
            hero = new Hero( Std.int(height*0.035), Std.int(height*0.035));
            addChild(hero);
            //hero.filter = new h2d.filter.Glow(0x00b4a3, 0.5, 500, 3, 100, true);
            //hero.bullets.filter = new h2d.filter.Glow(0x00b4a3, 0.5, 500, 4, 100, true);
            Chapter.getInstance().heroSpawn = true;
            return;
        }

        if ( Chapter.getInstance().heroSpawn ) {
            if (!Chapter.getInstance().moveKeySet) {
                hero.setMoveKey(key);
            }
            if (Chapter.getInstance().crateRecover && !Chapter.getInstance().gunKeySet) {
                hero.setGunKey(key);
            }
            if (Chapter.getInstance().crateRecover && hero.gunKey.length != 3 ) {

            }
            

        }

        keyBuffer.add(key);

    }

    public function randomSpawnPoint():Point {
        switch(Std.random(4)) {
            case 0:
                return new Point( Math.random(Window.getInstance().width), -50);
            case 1:
                return new Point( Math.random(Window.getInstance().width), Window.getInstance().height+50);
            case 2:
                return new Point( -50, Math.random(Window.getInstance().height));
            default:
                return new Point( Window.getInstance().width+50, Math.random(Window.getInstance().height));
        }
    }
     
}