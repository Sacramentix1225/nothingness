package addons;

class Upgrade {

    private static var instance:Upgrade;

    public static function getInstance():Upgrade {
        if ( instance == null ) {
            instance = new Upgrade();
        }
        return instance;
    }

    public var realControl:Bool = false;
    public var shader:Bool = false;
    public var soundEngine:Bool = false;
    public var HUD:Bool = false;
    public var gunLevel:Int = 0;

    public function new() {

    }
}