package addons;

import h2d.Text;
import pattern.Observer.NotifPack;
import pattern.Observer.Watcher;
import hxd.Res;
import motion.Actuate;
import story.Chapter;
import hxd.res.DefaultFont;
import addons.AddonsBox;
import h2d.Tile;
import h2d.Flow;
import h2d.Object;
import hxd.Window;
import hxd.Key as K;
import scene.Playground;
using pattern.Observer.Observables;

class AddonsSelector extends Object {
    
    public var flowBox:Flow;
    public var infoList:Array<AddonsBox>;
    public var selected:Int;

    private static var instance:AddonsSelector;

    public static function getInstance():AddonsSelector {
        if ( instance == null ) {
            var choiceList = new Array<AddonsBox>();
            var choice0 = new AddonsBox(DefaultFont.get(), "PLS give some real controls!", "the choice of noobs", 0);
            choiceList.push(choice0);
            var choice1 = new AddonsBox(DefaultFont.get(), "Add a sound engine", "don't expect that much from it", 1);
            choiceList.push(choice1);
            var choice2 = new AddonsBox(DefaultFont.get(), "Add some visual effect", "...", 2);
            choiceList.push(choice2);
            //var choice3 = new AddonsBox(DefaultFont.get(), "Add some texture", "quality is not garanted", 3);
            //choiceList.push(choice3);
            var choice3 = new AddonsBox(DefaultFont.get(), "I WANT TO UPGRADE MY GUN", "pff you S$!K", 3);
            choiceList.push(choice3);
            var choice4 = new AddonsBox(DefaultFont.get(), "I WANT STRONGER ENNEMIES", "nice you are a real xXProPlayerXx", 4);
            choiceList.push(choice4);
            var choice5 = new AddonsBox(DefaultFont.get(), "Where is the HUD ?", "real gamer play without HUD", 5);
            choiceList.push(choice5);
            instance = new AddonsSelector(choiceList);
        }
        return instance;
    }

    public function close() {
        this.remove();
        Actuate.timer(0.5).onComplete(function () {
            Chapter.getInstance().inUpgradeShop = false;
        });
    }


    public function new(infoList:Array<AddonsBox>, ?parent:Object) {
        super(parent);
        flowBox = new h2d.Flow(this);
		flowBox.layout = Vertical;
		flowBox.verticalSpacing = 1;
		flowBox.padding = 2;
        flowBox.maxWidth = Std.int(Window.getInstance().width*0.5);
        flowBox.maxHeight = Window.getInstance().height;
        flowBox.constraintSize(Window.getInstance().width*0.5,Window.getInstance().height);
        flowBox.fillHeight = true;
        flowBox.backgroundTile = Tile.fromColor(0x5d5d5d);
        flowBox.backgroundTile.scaleToSize(flowBox.outerWidth, flowBox.outerHeight);
        for (info in infoList) {
            info .alpha = 0.8;
            flowBox.addChild(info);
        }

        

    }

    public function update(dt:Float) {
        if (K.isPressed(K.DOWN)) {
            scroll(1);
        }
        if (K.isPressed(K.UP)) {
            scroll(-1);
        }
        if (K.isPressed(K.ENTER)) {
            get();
        }

    }

    public function scroll(direction:Int) {
        if (selected+direction < 1) {
            selected = 1;
            var temp = flowBox.getChildAt(flowBox.numChildren-1);
            flowBox.removeChild(flowBox.getChildAt(flowBox.numChildren-1));
            flowBox.addChildAt(temp,0);
        } else if (selected+direction > flowBox.numChildren-1 ) {
            selected = flowBox.numChildren-1;
            var temp = flowBox.getChildAt(1);
            flowBox.removeChild(flowBox.getChildAt(1));
            flowBox.addChild(temp);
        } else {
            selected+=direction;
        }
        
        applyFilter();

    }

    public function get() {
        //add gold check
        var choice = cast(flowBox.getChildAt(selected), AddonsBox).ID;
        switch (choice) {
            case 0:
                giveRealControl();
                flowBox.removeChild(flowBox.getChildAt(selected));
                
            case 1:
                giveSoundEngine();
                flowBox.removeChild(flowBox.getChildAt(selected));
            case 2:
                giveShader();
                flowBox.removeChild(flowBox.getChildAt(selected));
            case 5:
                giveHUD();
                flowBox.removeChild(flowBox.getChildAt(selected));
            case 4:
                upgradeGun();
                
        }
        close();
    }

    public function applyFilter() {
        for (info in flowBox) {
            info.alpha = 0.8;
        }
        flowBox.getChildAt(selected).alpha = 1;
    }

    public function giveRealControl() {
        Upgrade.getInstance().realControl = true;
    }

    public function giveSoundEngine() {
        Upgrade.getInstance().realControl = true;
    }
    public function giveShader() {
        Upgrade.getInstance().shader = true;
        Playground.hero.filter = new h2d.filter.Glow(0x20b2aa, 0.5, 20, 5, 1, true);
        Playground.hero.bullets.filter = new h2d.filter.Glow(0x20b2aa, 0.5, 20, 5, 1, true);
        Playground.ennemies.filter = new h2d.filter.Glow(0xbc3235, 0.5, 500, 4, 1, true);
        Playground.ennemies.tile = Tile.fromColor(0xac3235 ,alpha = 0.1);
         Playground.ennemies.tile = Tile.fromBitmap()
    }

    public function giveHUD() {
        Upgrade.getInstance().HUD = true;

        var heroHP = new Text(DefaultFont.get());
        heroHP.x = 5;
        heroHP.y = Window.getInstance().height - heroHP.textHeight -25;
        heroHP.textAlign = Left;
        heroHP.text = Playground.hero.hp + "/" + Playground.hero.maxhp;
        heroHP.textColor = 0xff0000;
        heroHP.scale(3);
        var callBack = function(notif:NotifPack):Any {
                if (notif.type == 0) heroHP.text = Std.string(notif.data + " / " + Playground.hero.maxhp);
                return null;
            }
        var hpw1 = new Watcher("HP", callBack);
        Playground.hero.addObserver(hpw1);

        Game.currentScene.addChild(heroHP);
    }

    public function upgradeGun() {
        Upgrade.getInstance().gunLevel++;
         Playground.hero.gunDamage += 5;
        switch (Upgrade.getInstance().gunLevel) {
            case 1:
                Playground.hero.gun.scale(1.5);
        }
    }

}