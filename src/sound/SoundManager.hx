package sound;

import hxd.snd.Channel;
import hxd.snd.Manager.Buffer;
import hxd.Res;

class SoundManager {

    private static var instance:SoundManager;
    private var music:Channel;
    //private static var musicRes:hxd.res.Sound;
    //private var currentMusic:Dynamic;

    public static function init() {
        //musicRes = if( hxd.res.Sound.supportedFormat(Mp3) || hxd.res.Sound.supportedFormat(OggVorbis) ) Res.sound.music.mainMusic else null;
        
        getInstance();
    }

    public static function getInstance() {
        if (instance==null) instance = new SoundManager();
        return instance;
    }

    private function new() {
        
        /*if( musicRes != null ) {
            //currentMusic = musicRes;
			//trace("Playing " + currentMusic)
    		music = musicRes.play(true);
			//music.queueSound(...);
			music.loop = true;
		}*/
    }
    public function trigger(soundFX:hxd.res.Sound) {
        var channel = soundFX.play(true);
        channel.loop = false;
    }

    
}